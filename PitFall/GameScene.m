//
//  GameScene.m
//  PitFall
//
//  Created by Vaster on 5/2/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import "GameScene.h"

@implementation GameScene {
    NSTimeInterval _lastUpdateTime;
    
    SKSpriteNode * background[10];
    SKSpriteNode * lava[10];
    SKSpriteNode * player;
    SKSpriteNode * platform[12];
    SKSpriteNode * resetButton;
    

    SKLabelNode * timeLabel;
    SKLabelNode * victoryLabel;
    SKLabelNode * defeatLabel;
    
    SKCameraNode * cam;
    
    //Player actions
    SKAction* jumpLeft;
    SKAction* walkLeft;
    SKAction* jumpRight;
    SKAction* walkRight;
    SKAction* terraVictory;
    SKAction* lavaBubble;
    
    SKTexture* terraRight;
    SKTexture* terraLeft;
    SKTexture* terraIdle;
    
    
    Boolean playerLeft;
    Boolean playerAlive;
    int frameCounter;
    int timeCounter;
    Boolean platformAlive[12];
    int timeRemaining;

}

- (void)sceneDidLoad {
    // Setup your scene here
    timeRemaining = 60;
    frameCounter = 0;
    timeCounter =0;
    
    static BOOL didLoad = NO;

    
    //Scene is being loaded twice for some reason, camera only works on second load
    //LOOKS INTO THIS AT SOME POINT BUT THIS IS A TEMP FIX
    if (didLoad)
    {
        cam = [SKCameraNode node];
        self.camera = cam;
        //NSLog(@"Skipping the second load");
        return;
    }
    didLoad = YES;

    // Initialize update time
    _lastUpdateTime = 0;

    self.physicsWorld.gravity = CGVectorMake( 0.0, -2.6 );

    
    [self makeBackground];
    [self makePlayer];
    [self makePlatforms];
    [self makeAnimation];
    [self makeLava];
    [self makeResetButton];
    [self makeTimeLabel];
    

}

-(void) makeBackground{
    
    for(int i = 0 ; i < 10; i++){
        background[i] = [SKSpriteNode spriteNodeWithImageNamed: @"Wall.png"];
        background[i].size = CGSizeMake(self.frame.size.width ,self.frame.size.height);
    
        [background[i] setPosition: CGPointMake((-1800) + (i * 400),0)];
        background[i].physicsBody.affectedByGravity = FALSE;
        [self addChild: (background[i])];
    }

    
}

-(void) makeTimeLabel{
    timeLabel = [SKLabelNode labelNodeWithFontNamed:@"ChalkboardSE-Bold"];
    timeLabel.position = CGPointMake(player.position.x-300, 600);
    timeLabel.fontSize = 100;
    timeLabel.fontColor = [SKColor redColor];
    timeLabel.name = @"timeLabel";
    
    NSString *timeLeft = [NSString stringWithFormat:@"%d",timeRemaining];
    timeLabel.text = timeLeft;
    
    [self addChild:timeLabel];

    
}

-(void) makeVictoryLabel{
    victoryLabel = [SKLabelNode labelNodeWithFontNamed:@"ChalkboardSE-Bold"];
    victoryLabel.position = CGPointMake(player.position.x, 200);
    victoryLabel.fontSize = 50;
    victoryLabel.fontColor = [SKColor whiteColor];
    victoryLabel.name = @"victoryLabel";
    
    NSString *victoryLeft = [NSString stringWithFormat:@"Thanks for your help friend!"];
    victoryLabel.text = victoryLeft;
    
    [self addChild:victoryLabel];
    
    
}

-(void) makeDefeatLabel{
    defeatLabel = [SKLabelNode labelNodeWithFontNamed:@"ChalkboardSE-Bold"];
    defeatLabel.position = CGPointMake(player.position.x, 200);
    defeatLabel.fontSize = 40;
    defeatLabel.fontColor = [SKColor whiteColor];
    defeatLabel.name = @"defeatLabel";
    
    NSString *defeatText= [NSString stringWithFormat:@"Game over! You lasted %d seconds" ,60 -timeRemaining];
    defeatLabel.text = defeatText;
    
    [self addChild:defeatLabel];
    
    
}

-(void) makeLava{
    for(int i = 0 ; i < 10; i++){
        lava[i] = [SKSpriteNode spriteNodeWithImageNamed: @"Lava1.png"];
        lava[i].size = CGSizeMake(self.frame.size.width ,100);
        
        [lava[i] setPosition: CGPointMake((-1800) + (i * 400),-620)];
        lava[i].physicsBody.affectedByGravity = FALSE;
        [self addChild: (lava[i])];
        [lava[i] runAction: lavaBubble];
    }
}

-(void) makePlayer{
    playerAlive = true;
    //Set up player
    terraIdle = [SKTexture textureWithImageNamed:@"Terra.gif"];
    player = [SKSpriteNode spriteNodeWithTexture: terraIdle];
    playerLeft = false;
    [player setPosition: CGPointMake(0,0)];
    player.size = CGSizeMake(80, 100);
    
    player.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:player.texture.size];
    player.physicsBody.dynamic = YES;
    player.physicsBody.allowsRotation = false;
    
    [self addChild: (player)];
}

-(void) makePlatforms {
    int xValue = -1000;
    for(int i = 0; i < 12; i++){
        platform[i] = [SKSpriteNode spriteNodeWithImageNamed:@"Bar.png"];
        platform[i].size = CGSizeMake(140, 40);
        [platform[i] setPosition:CGPointMake(xValue, -100)];
        
        platform[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(platform[i].size.width, 40)];
        platform[i].physicsBody.dynamic = NO;
        platform[i].physicsBody.affectedByGravity = false;
        platform[i].physicsBody.allowsRotation = false;

        [self addChild: (platform[i])];
        xValue = xValue + 150;
        
        platformAlive[i] = true;

    }

}

-(void) platformFall{
    int platNumber = arc4random_uniform(12);
    if(platformAlive[platNumber] != true){
        platNumber = arc4random_uniform(12);
    }
    platform[platNumber].physicsBody.dynamic = YES;

    platform[platNumber].physicsBody.affectedByGravity = TRUE;
}

-(void) makeAnimation{
    
    //Walk action Left
    SKTexture* TerraWalkLeft1 = [SKTexture textureWithImageNamed:@"TerraWalkLeft1.gif"];
    TerraWalkLeft1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraWalkLeft2 = [SKTexture textureWithImageNamed:@"TerraWalkLeft2.gif"];
    TerraWalkLeft2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraWalkLeft3 = [SKTexture textureWithImageNamed:@"TerraWalkLeft3.gif"];
    TerraWalkLeft3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraWalkLeft4 = [SKTexture textureWithImageNamed:@"TerraWalkLeft4.gif"];
    TerraWalkLeft4.filteringMode = SKTextureFilteringNearest;
    
    
    walkLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraWalkLeft1,TerraWalkLeft2,TerraWalkLeft3,TerraWalkLeft4] timePerFrame:0.2]];
    
    terraLeft = TerraWalkLeft1;
    
    
    //Walk action Right
    SKTexture* TerraWalkRight1 = [SKTexture textureWithImageNamed:@"TerraWalkRight1.gif"];
    TerraWalkRight1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraWalkRight2 = [SKTexture textureWithImageNamed:@"TerraWalkRight2.gif"];
    TerraWalkRight2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraWalkRight3 = [SKTexture textureWithImageNamed:@"TerraWalkRight3.gif"];
    TerraWalkRight3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraWalkRight4 = [SKTexture textureWithImageNamed:@"TerraWalkRight4.gif"];
    TerraWalkRight4.filteringMode = SKTextureFilteringNearest;
    
    
    walkRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraWalkRight1,TerraWalkRight2,TerraWalkRight3,TerraWalkRight4] timePerFrame:0.2]];
    
    terraRight = TerraWalkRight1;
    
    
    //Jump action Left
    SKTexture* TerraJumpLeft1 = [SKTexture textureWithImageNamed:@"TerraVictoryLeft.gif"];
    TerraJumpLeft1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraJumpLeft2 = [SKTexture textureWithImageNamed:@"TerraVictoryLeft2.gif"];
    TerraJumpLeft2.filteringMode = SKTextureFilteringNearest;
    
    jumpLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraJumpLeft1, TerraJumpLeft2] timePerFrame:0.2]];
    
    //Jump action Right
    SKTexture* TerraJumpRight1 = [SKTexture textureWithImageNamed:@"TerraJumpRight1.gif"];
    TerraJumpRight1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraJumpRight2 = [SKTexture textureWithImageNamed:@"TerraJumpRight2.gif"];
    TerraJumpRight2.filteringMode = SKTextureFilteringNearest;
    
    jumpRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraJumpRight1, TerraJumpRight2] timePerFrame:0.2]];
    
    //Victory Wave
    SKTexture* TerraWave1 = [SKTexture textureWithImageNamed:@"TerraWave1.tiff"];
    TerraWave1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraWave2 = [SKTexture textureWithImageNamed:@"TerraWave2.tiff"];
    TerraWave2.filteringMode = SKTextureFilteringNearest;
    
    terraVictory = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraWave1, TerraWave2] timePerFrame:0.2]];
    
    
    //Lava animation
    SKTexture* Lava1 = [SKTexture textureWithImageNamed:@"Lava1.png"];
    Lava1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* Lava2 = [SKTexture textureWithImageNamed:@"Lava2.png"];
    Lava2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* Lava3 = [SKTexture textureWithImageNamed:@"Lava3.png"];
    Lava3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* Lava4 = [SKTexture textureWithImageNamed:@"Lava4.png"];
    Lava4.filteringMode = SKTextureFilteringNearest;
    
    
    lavaBubble = [SKAction repeatActionForever:[SKAction animateWithTextures:@[Lava1,Lava2,Lava3,Lava4] timePerFrame:0.2]];
    
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    if ([node.name isEqualToString:@"reset"]) {
        [self reset];
    }
    else{
        if(playerAlive == true){
            if(location.y <= player.position.y){
                //if(player.physicsBody.velocity.dx == 0){
                if(location.x > player.position.x && location.y){
                    //[background[0] runAction: moveBackground];
                    playerLeft = false;
                    [player.physicsBody applyImpulse:CGVectorMake(4, 0)];
                    
                    [player runAction:walkRight];
                    
                }
                else{
                    playerLeft = true;
                    [player.physicsBody applyImpulse:CGVectorMake(-4, 0)];
                    [player runAction:walkLeft];
                    
                }
            }
            else{
                if(player.physicsBody.velocity.dy == 0){
                    if(location.x > player.position.x && location.y){
                        playerLeft = false;
                        [player.physicsBody applyImpulse:CGVectorMake(5, 30)];
                        [player runAction:jumpRight];
                        
                    }
                    else{
                        playerLeft = true;
                        [player.physicsBody applyImpulse:CGVectorMake(-5, 30)];
                        [player runAction:jumpLeft];
                        
                    }
                }
            }
        }
    
    }
    
    
}

-(void) makeResetButton {
    resetButton = [SKSpriteNode spriteNodeWithImageNamed:@"reset.png"];
    resetButton.size = CGSizeMake(150 ,100);
    resetButton.position = CGPointMake(player.position.x + 270,640);
    resetButton.name = @"reset";//how the node is identified later
    
    [self addChild: (resetButton)];
}


-(void) reset{
    [player removeFromParent];
    [victoryLabel removeFromParent];
    [defeatLabel removeFromParent];
    
    //Remove platfroms here
    for(int i = 0; i < 12; i++){
        [platform[i] removeFromParent];
    }
    
    [self makePlayer];
    [self makePlatforms];
    
    frameCounter = 0;
    timeCounter = 0;
    timeRemaining = 60;
}


-(void)update:(CFTimeInterval)currentTime {
    // Called before each frame is rendered
    
    // Calculate time since last update
    CGFloat dt = currentTime - _lastUpdateTime;
    
    // Update entities
    for (GKEntity *entity in self.entities) {
        [entity updateWithDeltaTime:dt];
    }
    
    // Initialize _lastUpdateTime if it has not already been
    if (_lastUpdateTime == 0) {
        _lastUpdateTime = currentTime;
    }
    
    cam.position = CGPointMake(player.position.x, CGRectGetMidY(self.frame));
    
    
    NSString *timeLeft = [NSString stringWithFormat:@"%d",timeRemaining];
    timeLabel.text = timeLeft;
    
    timeLabel.position = CGPointMake(player.position.x-300, 600);
    timeLabel.text = timeLeft;
    
    //Keep button on player
    
    resetButton.position = CGPointMake(player.position.x + 270, 620);
    
    if(player.physicsBody.velocity.dx == 0 && player.physicsBody.velocity.dy == 0 && playerAlive == true){
        [player removeAllActions];
        if(player.position.x < 20 && player.position.x > -20){
            [player setTexture: terraIdle];
        }
        else{
            if(playerLeft == true){
                [player setTexture: terraLeft];
            }else{
                [player setTexture:terraRight];
            }
        }
    }
    //printf("%f \n" , player.position.y);
    
    if(player.position.y < -620){

        playerAlive = false;
        [player removeAllActions];
        [player removeFromParent];
        [self makeDefeatLabel];
    }
    
    for(int i = 0; i < 12; i++){
        if(platform[i].position.y < -620){
            platformAlive[i] = false;
            [platform[i] removeFromParent];
        }
    }
    
    if (frameCounter == 30*4){
        if(timeRemaining > 0){
            [self platformFall];
            frameCounter = 0;
        }
        
    }
    if(timeCounter == 20 && playerAlive == true){
        if(timeRemaining > 0){
            timeRemaining--;
        }
        timeCounter = 0;
    }
    
    if(timeRemaining == 0 && playerAlive == true){
        playerAlive = false;
        [player removeAllActions];
        [player runAction:terraVictory];
        [self makeVictoryLabel];
        
    }

    
    _lastUpdateTime = currentTime;
    frameCounter++;
    timeCounter ++;
}

@end
